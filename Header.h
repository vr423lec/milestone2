#ifndef HEADER_H
#define HEADER_H

#include <ctime>

#include <algorithm>
#include <cmath>
#include <cstring>
#include <vector>
#include <sstream>
#include <queue>
#include <map>
#include <set>
#include <gl/glew.h>
#include <glm.hpp>

#include <gtc\matrix_transform.hpp>
#include <gtc\type_ptr.hpp>

#define BYTE unsigned char
#define UINT unsigned int


#endif