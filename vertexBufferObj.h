#pragma once
#ifndef VERTEXBUFFEROBJ_H
#define VERTEXBUFFEROBJ_H
#include "Header.h"

class vertexBufferObj
{
public:
	vertexBufferObj(int size = 0);
	void createVBO(int size = 0);
	void deleteVBO();
	void bindVBO(int type);
	void uploadData(int drawMode);
	void add_data(void* ptr, unsigned int dataSize);
	int getCurrentSize();
	int getID();

private:
	std::vector<BYTE> data;
	int current_size;
	int bufferType;
	GLuint buffer;
};

#endif