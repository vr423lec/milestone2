#include "Simulator.h"
GLuint screenWidth = 800, screenHeight = 600;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;
GLfloat last_frame = glfwGetTime();
int FPS = 0;
int nbFrames = 0;
int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "Simulator", nullptr, nullptr);
	glfwMakeContextCurrent(window);

	// Options
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	Simulator simulator(window);

	while (!glfwWindowShouldClose(window))
	{
		GLfloat currentFrame = glfwGetTime();
		nbFrames++;
		if (currentFrame - last_frame >= 1.0f)
		{
			FPS = double(nbFrames);
			nbFrames = 0;
			last_frame += 1.0f;
		}
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		simulator.Go(deltaTime, FPS);
		glfwPollEvents();
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, GL_TRUE);
	}

	glfwTerminate();

	return 0;
}