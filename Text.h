#pragma once
#include "Header.h"
#include "shader.h"
#include <ft2build.h>
#include "vertexBufferObj.h"
#include FT_FREETYPE_H

struct Character {
	GLuint TextureID;
	glm::ivec2 Size;
	glm::ivec2 Bearing;
	GLuint Advance;
};

class Text
{
public:
	Text();
	~Text();

	void setup();
	void render_text(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);

private:
	FT_Library ft;
	FT_Face face;
	Shader textShader;
	GLuint VBO;
	unsigned int VAO;
	GLuint texture;
	std::map<GLchar, Character> Characters;
};
