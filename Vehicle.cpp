#include "Vehicle.h"

Model Vehicle::vehicleModel;

Vehicle::Vehicle()
{
	speed = 0.0f;
	targetSpeed = 60.0f;
	posX = 0.0f;
	posY = 0.0f;
	startX = 0.0f;
	acc = 0.0f;
	change = 1;
}

void Vehicle::loadModel()
{
	vehicleModel.create("C:/Users/Victor/Desktop/Project/Car/BMW_M3_GTR.obj");
}

void Vehicle::Render(Shader shader)
{
	glm::mat4 model2;
	//model2 = glm::rotate(model2, glm::radians(180.f), glm::vec3(0.0f, 1.0f, 0.0f));
	model2 = glm::translate(model2, glm::vec3(posX + startX, -1.75f, posY));
	model2 = glm::scale(model2, glm::vec3(0.001f, 0.001f, 0.001f));
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
//	cout << spd << std::endl;
	vehicleModel.Draw(shader);
}

float Vehicle::getPositionX()
{
	return startX + posX;
}

float Vehicle::getPositionY()
{
	return posY;
}

void Vehicle::change_lane_right(Shader shader, float t)
{
	if (posY >= 12.0f) return;
	glm::mat4 model2;
	model2 = glm::translate(model2, glm::vec3(0.0f, 0.0f, 12.0f*t));
	posY = posY + 12.0f*t;
	if (posY <= 12.5f && posY >= 11.5f) posY = 12.0f;
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
}

void Vehicle::set_position(float x, float y)
{
	startX = x;
	posY = y;
}

void Vehicle::change_lane_left(Shader shader, float t)
{
	if (posY <= -12.0f) return;
	glm::mat4 model2;
	model2 = glm::translate(model2, glm::vec3(0.0f, 0.0f, -12.0f*t));
	posY = posY - 12.0f*t;
	if (posY >= -12.5f && posY <= -11.5f) posY = -12.0f;
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
}

void Vehicle::change_lane_center(Shader shader, float t)
{
	if (posY < 0.0f) change_lane_right(shader, t);
	else if (posY > 0.0f) change_lane_left(shader, t);
	if (posY < 0.5f && posY > -0.5f) posY = 0.0f;
} 

void Vehicle::accelerate(float t)
{
	acc = acc + 20.0f*t;
	speed = speed + acc * t;
	if (speed >= targetSpeed)
	{
		speed = targetSpeed;
		acc = 0.0f;
	}
}

void Vehicle::brake(float t)
{
	acc = acc - 5.0f*t;
	speed = speed + acc*t;
	if (speed <= targetSpeed)
	{
		speed = targetSpeed;
		acc = 0.0f;
	}
}

void Vehicle::set_targetSpeed(float tSpeed)
{
	targetSpeed = tSpeed;
}

float Vehicle::getSpeed()
{
	return speed;
}

float Vehicle::getTargetSpeed()
{
	return targetSpeed;
}

void Vehicle::motion(float t)
{
	if (targetSpeed > speed) accelerate(t);
	if (targetSpeed < speed) brake(t);
	posX = posX + speed * t;
}

void Vehicle::set_speed(float spd)
{
	speed = spd;
}

void Vehicle::reset()
{
	posX = 0.0f;
	startX = 0.0f;
}
