#pragma once
#include "GLGraphics.h"
#include "Vehicle.h"
#include "road.h"
#include "Text.h"

struct position
{
	float pX, pY;
};

class Simulator
{
public:
	Simulator(GLFWwindow* win);
	~Simulator();
	void Go(float deltatime, int FPS);

private:
	void CreateFrame();
	void traffic_control(float deltatime);
	void manageTraffic();

	bool checkIfSafeL(Vehicle trVeh);
	bool checkIfSafeR(Vehicle trVeh);
	bool checkIfSafeC(Vehicle trVeh);

	void menu();
	void manage_menu();

	int checkForInput();
private:
	GLFWwindow* window;
	GLGraphics gfx;
	Shader shader;
	Vehicle* ourVehicle;
	Vehicle* newVeh;
	vector <Vehicle*> traffic;
	float randomVar , trSwitchLane, randVar;
	float ourSpeed, trSpeed;
	road ourRoad;
	int rnd;
	bool test;
	Text screenText;
	int currentMenuState, subMenuState;
	string number;
	int counter, checkADD = 0;
	float cursorX, cursorY;
};
