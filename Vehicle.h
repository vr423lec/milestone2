#pragma once
#include "Model.h"
#include "Header.h"
#include <GLFW/glfw3.h>

class Vehicle
{
public:
	Vehicle();
	void Render(Shader shader);
	void loadModel();
	float getPositionX();
	float getPositionY();
	void change_lane_right(Shader shader, float t);
	void change_lane_left(Shader shader, float t);
	void change_lane_center(Shader shader, float t);
	void set_position(float x, float y);
	void accelerate(float t);
	void brake(float t);
	void set_targetSpeed(float tSpeed);
	float getSpeed();
	float getTargetSpeed();
	void motion(float t);
	void set_speed(float spd);
	void reset();

	static Model vehicleModel;
	int change, usrCMD = 0;
	bool usrChLane = false;
	
private:
	float speed, targetSpeed;
	float posX, posY;
	float startX;
	float acc;
};
