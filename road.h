#pragma once
#include "Header.h"
#include "vertexBufferObj.h"
#include "TextureHandler.h"
#include "shader.h"
#include <glm.hpp>
using namespace glm;

struct point
{
	float x, y, z = 0.0f;
};



class road
{
public:
	road();
	~road();
	void GenRoad(Shader sh, point p1, point p2, point p3, point p4, float index);
	void Render(Shader shader, float index);
	void setup(Shader sh);


private:
	vertexBufferObj vertexObj, EBO;
	TextureHandler RoadTexture;
	GLuint VAO;
	float x,y,z,a;
};


